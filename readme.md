# Entrevista Notus

Esta es una pre-entrevista para la oferta de trabajo de Notus. Actualmente, tenemos varios proyectos desarrollados en Django, y por ende, desarrollamos un par de requerimientos para conocer un poco más de tus capacidades técnicas.


## El proyecto

En el repositorio se puede encontrar una aplicación simple para modelar un terminal, el que tiene un estanque de producto que puede ser disminuida por una demanda (requerida por una distribuidora) o reabastecido por la descarga de un barco. En la vista principal se puede encontrar una tabla en donde se tiene la demanda, descarga de barcos e inventario final diario de cada terminal.

Al correr las migraciones se cargarán datos por defectos para poder probar lo que están haciendo.

Finalmente, para ver el desempeño de la operación necesitamos realizar dos tareas:

- Una tabla con la descarga mensual del barco en cada terminal. (ships/:id)
- Un gráfico del inventario del terminal y las llegadas de los barcos. (terminals/:id)
- Un gráfico de los 5 distribuidores que más producto han demandado. (terminals/:id/distributors)
- Una tabla con la demanda mensual a cada terminal. (distributors/:id)

Les pedimos que nos envíen su solución comprimida, en conjunto a un texto explicando que hicieron y por qué, a [contacto@notus.cl](mailto:contacto@notus.cl).
