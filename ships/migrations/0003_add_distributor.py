from django.db import migrations, models
import django.db.models.deletion
import random


def create_distributors(apps, schema_editor):
    Distributor = apps.get_model('ships', 'Distributor')
    names = [
        'High Spirits', 'The Grog Bog', 'Toddie’s Tonics', 'On The Rocks Liquor', 'Laguna Liquor', 'Lavish Liquors',
        'Sanguine Spirits', 'Spirits & Scallywags', 'Siduri Spirits', 'Pan’s Precious Beverages', 'Brandy’s Bourbon',
        'Kahlua & Schnapps', 'Sam’s Sambuca', 'Scotch & Sizzle', 'Whiskey On The Wind', 'Drinks & Delight',
        'Mama’s Martinis', 'Bits & Barley', 'Growlers & Goods', 'Chum’s Rums',
    ]
    distributors = list(map(lambda x: Distributor(name=x), names))
    Distributor.objects.bulk_create(distributors)


def add_distributors_to_demanda(apps, schema_editor):
    Distributor = apps.get_model('ships', 'Distributor')
    Demand = apps.get_model('ships', 'Demand')
    
    distributors = Distributor.objects.all()
    # add to demanda
    random.seed(1111)
    for demand in Demand.objects.all():
        distributor = random.choice(distributors)
        demand.distributor = distributor
        demand.save()


class Migration(migrations.Migration):

    dependencies = [
        ('ships', '0002_initial_data'),
    ]

    operations = [
        migrations.CreateModel(
            name='Distributor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'ordering': ['created_at'],
                'abstract': False,
            },
        ),
        migrations.RunPython(create_distributors),
        migrations.AddField(
            model_name='demand',
            name='distributor',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='ships.Distributor'),
        ),
        migrations.RunPython(add_distributors_to_demanda),
    ]
